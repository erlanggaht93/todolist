import { notFound } from "next/navigation";

// GET
const getTodoList = async () => {
  try {
    const response = await fetch(
      `${process.env.NEXT_PUBLIC_URL_ENDPOINT}/${process.env.NEXT_PUBLIC_POSTS}`, {"cache":"no-cache"}
      );
      if(response.status >= 400 && response.status <= 499)throw new Error
      return response.json();
    } catch (error) {
       throw new Error(notFound())
  }
};

// ADD
const addTodoList = async (body) => {
  try {
    const response = await fetch(`${process.env.NEXT_PUBLIC_URL_ENDPOINT}/${process.env.NEXT_PUBLIC_POSTS}`,{
      method : "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(body)
    })
    return response
  } catch (error) {
    throw new Error(error);
  }
}

// UPDATE 
const updateTodoList = async (body,id) => {
  try {
    const response = await fetch(`${process.env.NEXT_PUBLIC_URL_ENDPOINT}/${process.env.NEXT_PUBLIC_POSTS}/${id}`,{
      method : "PUT",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(body)
    })
    return response
  } catch (error) {
    throw new Error(error);
  }
}

// DELETE
const deleteTodoList = async (id) => {
  try {
    const response = await fetch(`${process.env.NEXT_PUBLIC_URL_ENDPOINT}/${process.env.NEXT_PUBLIC_POSTS}/${id}`,{method:'DELETE'});
    return response.json();
  } catch (error) {
    throw new Error(error);
  }
};

export { getTodoList, deleteTodoList,addTodoList,updateTodoList };
