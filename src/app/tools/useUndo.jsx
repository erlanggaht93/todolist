import { useEffect, useRef } from "react";

export default function useUndo(setTodo,key,todo) {
    const ctrlPressed = useRef(false);
    
    useEffect(() => {
    if(todo.length <= 0) return 
    const handleKeyDown = (event) => {

      // Cek apakah tombol Ctrl ditekan
      if (event.key === 'Control') {
        ctrlPressed.current = true;
      }

      // Cek apakah tombol Z ditekan dan Ctrl juga ditekan
      if (event.key === key && ctrlPressed.current) {
        setTodo(prev => {
          let copyTodo = [...prev]
          copyTodo.pop()
          return copyTodo
        })
        }
    };

    const handleKeyUp = (event) => {
      // Setel kembali ke false saat tombol Ctrl dilepaskan
      if (event.key === 'Control') {
        ctrlPressed.current = false;
      }
    };

    // Menambahkan event listener saat komponen dipasang
    document.addEventListener('keydown', handleKeyDown);
    document.addEventListener('keyup', handleKeyUp);

    // Membersihkan event listener saat komponen dibongkar
    return () => {
      document.removeEventListener('keydown', handleKeyDown);
      document.removeEventListener('keyup', handleKeyUp);
    };
  }, [todo]);
}
