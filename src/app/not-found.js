import Link from "next/link";

async function NotFound() {
  return (
    <div className="grid place-content-center mx-auto h-[92vh] text-center">
      there is an error on the server or the page was not found !
      <br />
      <Link href={"/"}>
        <button
          type="button"
          className="text-white  mt-4 bg-blue-700 hover:bg-blue-800  font-medium rounded-lg text-sm px-5 py-2.5  focus:outline-none "
        >
          Back
        </button>
      </Link>
    </div>
  );
}

export default NotFound;
