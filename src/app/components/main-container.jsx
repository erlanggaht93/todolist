function MainContainer({children}) {
  return <main className="w-1/2 mx-auto  py-6 relative">{children}</main>
}

export default MainContainer
