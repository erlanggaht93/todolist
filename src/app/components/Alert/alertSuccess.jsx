import GlobalContext from '@/global-context/globalContext'
import PropTypes from 'prop-types'
import { useContext } from 'react'

function AlertSuccess({props}) { 
  const {alertContext} = useContext(GlobalContext)
  const {styleClass,stringSuccess}= props
  return (
    <div role="alert" className={`rounded-xl border border-gray-100 bg-white p-4 ${styleClass}`}>
  <div class="flex items-start gap-4">
    <span class="text-green-600">
      <svg
        xmlns="http://www.w3.org/2000/svg"
        fill="none"
        viewBox="0 0 24 24"
        strokeWidth="1.5"
        stroke="currentColor"
        class="h-6 w-6"
      >
        <path
          stroke-linecap="round"
          stroke-linejoin="round"
          d="M9 12.75L11.25 15 15 9.75M21 12a9 9 0 11-18 0 9 9 0 0118 0z"
        />
      </svg>
    </span>

    <div class="flex-1">
      <strong class="block font-medium text-gray-900"> {stringSuccess} </strong>

    </div>

    <button class="text-gray-500 transition hover:text-gray-600" onClick={() => {
      alertContext.setAlertSave(false)
      alertContext.setAlertClear(false)
    }}>
      <span class="sr-only">Dismiss popup</span>

      <svg
        xmlns="http://www.w3.org/2000/svg"
        fill="none"
        viewBox="0 0 24 24"
        strokeWidth="1.5"
        stroke="currentColor"
        class="h-6 w-6"
      >
        <path
          stroke-linecap="round"
          stroke-linejoin="round"
          d="M6 18L18 6M6 6l12 12"
        />
      </svg>
    </button>
  </div>
</div>

  )
}


export default  AlertSuccess;

AlertSuccess.propTypes = {
  props : PropTypes.shape ({
    styleClass: PropTypes.string,
    stringSuccess : PropTypes.string   
  })
}