import GlobalContext from '@/global-context/globalContext'
import PropTypes from 'prop-types'
import { useContext } from 'react'

function AlertFailed({props}) { 
  const {alertContext} = useContext(GlobalContext)
  const {styleClass,stringFailed}= props
  return (
 <div role="alert" class={`rounded border-s-4 border-red-500 bg-red-50 p-2 ${styleClass}`}>
  <strong class="block font-medium text-red-800"> Something went wrong </strong>

  <p class="mt-2 text-sm text-red-700">
   {stringFailed}
  </p>
</div>
  )
}


export default  AlertFailed;

AlertFailed.propTypes = {
  props : PropTypes.shape ({
    styleClass: PropTypes.string,
    stringFailed : PropTypes.string   
  })
}