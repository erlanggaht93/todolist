import React from "react";
import PropTypes, { string } from "prop-types";
function KBD({ props }) {
  const { styleClass, key, desc = { active: false, string: "" } } = props;

  return (
    <p className={`text-gray-400 dark:text-gray-400 ${styleClass}`}>
      {desc.active && <span className="lowercase">{desc.string}</span>}
      {key.active === 1 && (
        <>
          <kbd className="px-2 py-1.5 text-xs font-semibold text-gray-500  border border-gray-200 rounded-lg dark:bg-gray-600 dark:text-gray-100 dark:border-gray-500 shadow">
            {key.string1}
          </kbd>
        </>
      )}
      {key.active === 2 && (
        <>
          <kbd className="px-2 py-1.5 text-xs font-semibold text-gray-500  border border-gray-200 rounded-lg dark:bg-gray-600 dark:text-gray-100 dark:border-gray-500">
            {key.string1}
          </kbd>
          <kbd className="px-2 py-1.5 text-xs font-semibold text-gray-500  border border-gray-200 rounded-lg dark:bg-gray-600 dark:text-gray-100 dark:border-gray-500 shadow">
            {key.string2}
          </kbd>
        </>
      )}
    </p>
  );
}

KBD.propTypes = {
  props: PropTypes.shape({
    styleClass: PropTypes.string,
    key: PropTypes.shape({
      active: PropTypes.number,
      string1: PropTypes.string,
      string2: PropTypes.string,
    }),
    desc: PropTypes.shape({
      active: PropTypes.bool,
      string: PropTypes.string,
    }),
  }),
};

export default KBD;
