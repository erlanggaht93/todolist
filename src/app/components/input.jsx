import PropTypes from 'prop-types';
import KBD from '@/components/kbd';

 function Input({props}) {
    const {withlabel,textlabel,placeholder,ref} = props
    
  return (
    <div>

  {/* withLabel */}
   {!withlabel === 'false' ?
   <label htmlFor="todo" className="block text-xs font-medium text-gray-700">{textlabel}</label> : undefined
   } 

  {/* withKBD  */}
  {}
  <div className='relative'>
  <KBD props={{styleClass:'absolute top-3 right-2 scale-75',key:{active:1, string1:'ENTER'}}}/>  
  <input
    type="text"
    id="todo"
    placeholder={placeholder}  className="w-full rounded-md border-gray-200 p-2 my-2 shadow sm:text-sm"
    {...props}
  />
  </div>

</div>
  )
}

Input.propTypes = {
  props: PropTypes.shape ({
    withlabel: PropTypes.bool && PropTypes.string,
    textlabel: PropTypes.string,
    placeholder: PropTypes.string,
  })
}


export default Input;

