"use client";
import Input from "@/components/input";
import { useCallback, useContext, useEffect, useState } from "react";
import KBD from "./kbd";
import { ComponentEditDelete } from "@/(page)/client/page";
import {
  addTodoList,
  deleteTodoList,
  getTodoList,
  updateTodoList,
} from "@/tools/API/fetchTodoList";
import AlertSuccess from "./Alert/alertSuccess";
import AlertFailed from "./Alert/alertFailed";
import GlobalContext from "@/global-context/globalContext";
import {  usePathname, useRouter, useSearchParams } from "next/navigation";

const   ClientComponentInServer = ({ datas }) => {
  const searchParams = useSearchParams()
  const pathName = usePathname()
  const router = useRouter()

  // Create Query Params String
  const params = new URLSearchParams(searchParams)
  const getCreateQueryString = useCallback((name,value) => {
    const params = new URLSearchParams(searchParams)
    params.set(name, value)
    return params.toString()                                       
    },[searchParams])


  const { alertContext } = useContext(GlobalContext);
  const [inputTodo, setInputTodo] = useState("");
  const [todo, setTodo] = useState(datas);

  // Active Method
  const [addDone, setAddDone] = useState(false);
  const [deleteDone, setDeleteDone] = useState(false);
  const [updateDone, setUpdateDone] = useState(false) 

  //Refatch Todo
  useEffect(() => {
    (async function getTodoAgain() {
      if (deleteDone || addDone || updateDone) {
        const response = await getTodoList();
        setTodo(response);
      }
    })();

    return () => {
      setAddDone(false);
      setDeleteDone(false);
      setUpdateDone(false);
      router.push(pathName)
    };
  }, [addDone, deleteDone,updateDone]);

  // Method Add Todo
  const addTodo = (e) => {
    const inputNotNull = inputTodo === "";
    if (e.key === "Enter") {
       
      // Edit
       if(activeEdit) {
        if (inputEdit === '') return alertContext.setAlertFailed({active:true,duration:1500}) 
        updateRequest({'todo':inputEdit}, params.get('id'))
        setInputEdit('')
        setActiveEdit(false)
        return  
      }

      if (inputNotNull)
        return alertContext.setAlertFailed({
          active: true,
          duration: 1500,
        });
      const bodyJson = { 'todo': inputTodo };
      addTodoList(bodyJson)
        .then(() =>setAddDone(true))
        .catch(() =>
          alertContext.setAlertFailed({ active: true, duration: 1500 })
        );
    }
  };

  //Method Edit
  const [activeEdit,setActiveEdit] = useState(false)
  const [inputEdit,setInputEdit] = useState('')
  const editTodo = useCallback((value,id) => {
    router.push(pathName + '?' + getCreateQueryString('id',id))
    localStorage.setItem('update',value.todo)
    setActiveEdit(true)
    setInputEdit(value.todo)
    setInputTodo('')
  },[inputEdit])
  const updateRequest = async (todo,id) => {
    const response =  await updateTodoList(todo,id)
    if (response.status === 200) setUpdateDone(true)
    console.log(response)

  }


  // Method Delete Todo
  const deleteTodo = (id) => {
    setDeleteDone(true);
    deleteTodoList(id)
  };

  return (
    <>
      {/* Alert */}
      {alertContext.alertSave && (
        <AlertSuccess
          props={{
            styleClass: " w-full absolute",
            stringSuccess: "Saved Successfully",
          }}
        />
      )}
      {alertContext.alertClear && (
        <AlertSuccess
          props={{
            styleClass: " w-full absolute ",
            stringSuccess: "Clear Successfully",
          }}
        />
      )}
      {alertContext.alertFailed && (
        <AlertFailed
          props={{
            styleClass: " w-full absolute ",
            stringFailed: 'Todo must be filled in',
          }}
        />
      )}
      <h1 className="text-center mt-20 text-xl drop-shadow">
        Todo List - Server
      </h1>
      {activeEdit ? 
      <Input props={{
        withlabel:'false',
        textlabel:"Todolist",
        placeholder:"Update to-do ( press ESC to cancel )",
        onChange:(e) => setInputEdit(e.target.value),
        value: inputEdit,
        onKeyDown:(e) => addTodo(e),
      }}
      />
      :  
      <Input props={{
        withlabel:'false',
        textlabel:"Todolist",
        placeholder:"Write new to-do",
        onChange:(e) => setInputTodo(e.target.value),
        value: inputTodo,
        onKeyDown:(e) => addTodo(e),
      }}
      /> }


      <div className="area-list px-1 mt-4 max-h-[350px] overflow-y-auto">
        {todo.map((m, i) => {
          return (
            <li key={i} className="group">
              <p>{m.todo}</p>
              <ComponentEditDelete
                todo={todo}
                deleteTodo={() => deleteTodo(m.id)}
                editTodo={() => editTodo(m,m.id)}
              />
            </li>
          );
        })}
      </div>
      <div
        id="saveActions"
        className={`text-right ${
          todo.length > 0 && "border-t flex justify-end gap-2"
        }`}
      >
        {todo.length > 0 ? (
          <>
            {/* <button
              type="button"
              className="text-gray-700  mt-4 border hover:bg-gray-100  font-medium rounded-lg text-sm px-5 py-2.5   focus:outline-none "
              onClick={() => clearTodo()}
            >
              Clear Todo{" "}
            </button> */}
          </>
        ) : (
          <p className="text-gray-400 ">No Todo ...</p>
        )}
      </div>

      <KBD
        props={{
          styleClass: "fixed bottom-6 left-5 scale-90",
          key: {
            active: 2,
            string1: "CTRL",
            string2: "Z",
          },
          desc: {
            active: true,
            string: "Undo Todo",
          },
        }}
      />
    </>
  );
};

export default ClientComponentInServer;
