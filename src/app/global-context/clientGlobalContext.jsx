'use client'
import { useState } from "react";
import GlobalContext from "./globalContext";
function ClientGlobalContext({children}) {
  return( 
    <GlobalContext.Provider value={{alertContext: alertContext()}}>
        {children}
    </GlobalContext.Provider>
  )
}


export default ClientGlobalContext;

export const alertContext = () => {
    const [alertSave, setAlertSave] = useState({
        active: false,
        duration: 1200
    });
    const [alertClear, setAlertClear] = useState({
        active: false,
        duration: 1200
    });
    const [alertFailed, setAlertFailed] = useState({
        active: false,
        duration: 1200
    });

    if(alertSave.active) setTimeout(()=> setAlertSave({active:false,duration:1200}),alertSave.duration)
    if(alertClear.active) setTimeout(()=>setAlertClear({active:false,duration:1200}),alertClear.duration)
    if(alertFailed.active) setTimeout(()=>setAlertFailed({active:false,duration:1200}),alertFailed.duration)

    return {
        alertSave: alertSave.active,
        alertClear: alertClear.active,
        alertFailed: alertFailed.active,
        setAlertSave,
        setAlertClear,
        setAlertFailed,
    }

}