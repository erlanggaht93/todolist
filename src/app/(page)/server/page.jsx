import ClientComponentInServer from "@/components/clientComponentInServer";
import { getTodoList } from "@/tools/API/fetchTodoList"

async function Server() {
  const datas = await getTodoList()
  
  return(
    <ClientComponentInServer datas={datas}/>
  )
  
}

export default Server;
