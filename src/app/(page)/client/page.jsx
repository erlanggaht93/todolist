"use client";
import { useCallback, useContext, useEffect, useState } from "react";
import Input from "@/components/input";
import useUndo from "@/tools/useUndo";
import KBD from "@/components/kbd";
import AlertSuccess from "@/components/Alert/alertSuccess";
import GlobalContext from "@/global-context/globalContext";
import AlertFailed from "@/components/Alert/alertFailed";
import { usePathname } from "next/navigation";

function ClientTodoList() {
  const {alertContext} = useContext(GlobalContext)
  const [inputTodo, setInputTodo] = useState("");
  const [todo, setTodo] = useState([]);

  //Method Edit
  const [activeEdit,setActiveEdit] = useState(false)
  const [inputEdit,setInputEdit] = useState('')
  const editTodo = useCallback((value,index) => {
    localStorage.setItem('update',value)
    setActiveEdit(true)
    setInputEdit(value)
    setInputTodo('')
  },[inputEdit])

  // Custom hooks
  useUndo(setTodo, "z",todo); // replace parameter two with keyboard event
  
  // Cancel Update
  useEffect(() => {
    if(activeEdit === true) {
      const handleKeyCancel = (event) => {
        if (event.key === 'Escape') {
          setActiveEdit(false)
        }
      }
      document.addEventListener('keydown',handleKeyCancel)

      return () => {
        document.removeEventListener('keydown',handleKeyCancel)
      }
    }
  },[activeEdit])

  
  // Method add todo use submit enter
  const addTodo = (e) => {
    const inputNotNull = inputTodo === ""
    if (e.key === "Enter") {

      // Edit
      if(activeEdit) {
        if (inputEdit === '') return alertContext.setAlertFailed({active:true,duration:1500}) 
        let copyTodo = [...todo]
        const updateValueInput= localStorage.getItem('update')
        const findIndex = copyTodo.indexOf(updateValueInput)
        copyTodo[findIndex] = inputEdit
        setTodo(copyTodo)
        setActiveEdit(false)
        return  
      }
      // Add
      if(inputNotNull) return alertContext.setAlertFailed({active:true,duration:1500})
      setTodo([...todo, inputTodo]);
      setInputTodo("");
    }
  };

  // Method deleteTodo and clearTodo
  const deleteTodo = useCallback((i) => {
      setTodo((prev) => {
        let deleteItem = prev.filter((item, index) => index !== i);
        return deleteItem;
      });
    },
    [todo, inputTodo]
  );
  const clearTodo = () => {
    if (confirm("Are you sure you want to delete all to-dos?")) {
      setTodo([]);
      localStorage.setItem("todoStorage", JSON.stringify([]));
      alertContext.setAlertClear({active:true,duration:2400})
    }
  };

  // Method SaveTodo and Clear
  const saveTodo = () => {
    localStorage.setItem("todoStorage", JSON.stringify(todo));
    alertContext.setAlertSave({active:true,duration:2400})
  }
  useEffect(() => {
    const getTodoStorage = localStorage.getItem("todoStorage");
    if (!getTodoStorage)
      localStorage.setItem("todoStorage", JSON.stringify([]));
    setTodo(JSON.parse(getTodoStorage));
  }, []);

  return (
    <>
      {/* Alert */}
      {alertContext.alertSave && <AlertSuccess props={{ styleClass: " w-full absolute",stringSuccess:"Saved Successfully" }} />}
      {alertContext.alertClear && <AlertSuccess props={{ styleClass: " w-full absolute ",stringSuccess:"Clear Successfully" }} />}
      {alertContext.alertFailed && <AlertFailed props={{ styleClass: " w-full absolute ",stringFailed:"Todo must be filled in" }} />}


      <h1 className="text-center mt-20 text-xl drop-shadow">
        Todo List - Client
      </h1>
      {activeEdit ? 
      <Input props={{
        withlabel:'false',
        textlabel:"Todolist",
        placeholder:"Update to-do ( press ESC to cancel )",
        onChange:(e) => setInputEdit(e.target.value),
        value: inputEdit,
        onKeyDown:(e) => addTodo(e),
      }}
      />
      :  
      <Input props={{
        withlabel:'false',
        textlabel:"Todolist",
        placeholder:"Write new to-do",
        onChange:(e) => setInputTodo(e.target.value),
        value: inputTodo,
        onKeyDown:(e) => addTodo(e),
      }}
      /> }

      <div className="area-list px-1 mt-4 max-h-[350px] overflow-y-auto  ">
        {todo.map((m, i) => {
          return (
            <li key={i} className="group ">
              <p>
                {m}
              </p>
              <ComponentEditDelete todo={todo} editTodo={() => editTodo(m,i)} deleteTodo={() => deleteTodo(i)} />
            </li>
          );
        })}
      </div>
      <div
        id="saveActions"
        className={`text-right ${
          todo.length > 0 && "border-t flex justify-end gap-2"
        }`}
      >
        {todo.length > 0 ? (
          <>
            <button
              type="button"
              className="text-white  mt-4 bg-blue-700 hover:bg-blue-800  font-medium rounded-lg text-sm px-5 py-2.5  focus:outline-none "
              onClick={() => saveTodo()}
            >
              Save Todo{" "}
            </button>
            <button
              type="button"
              className="text-gray-700  mt-4 border hover:bg-gray-100  font-medium rounded-lg text-sm px-5 py-2.5   focus:outline-none "
              onClick={() => clearTodo()}
            >
              Clear Todo{" "}
            </button>
          </>
        ) : (
          <p className="text-gray-400 ">No Todo ...</p>
        )}
      </div>

      <KBD
        props={{
          styleClass: "fixed bottom-6 left-5 scale-90",
          key: {
            active: 2,
            string1: "CTRL",
            string2: "Z",
          },
          desc: {
            active: true,
            string: "Undo Todo",
          },
        }}
      />
    </>
  );
}

export default ClientTodoList;

export const ComponentEditDelete = ({ editTodo,deleteTodo,todo }) => {
  const pathname = usePathname()

  return (
    <div>
      <button className="inline-flex items-center gap-2 rounded-md mr-4 py-2 text-sm text-gray-500 group-hover:text-sky-500 focus:relative"
      onClick={editTodo}
      >
        <svg
          xmlns="http://www.w3.org/2000/svg"
          fill="none"
          viewBox="0 0 24 24"
          strokeWidth="1.5"
          stroke="currentColor"
          className="h-4 w-4"
        >
          <path
            strokeLinecap="round"
            strokeLinejoin="round"
            d="M16.862 4.487l1.687-1.688a1.875 1.875 0 112.652 2.652L10.582 16.07a4.5 4.5 0 01-1.897 1.13L6 18l.8-2.685a4.5 4.5 0 011.13-1.897l8.932-8.931zm0 0L19.5 7.125M18 14v4.75A2.25 2.25 0 0115.75 21H5.25A2.25 2.25 0 013 18.75V8.25A2.25 2.25 0 015.25 6H10"
          />
        </svg>
        Edit
      </button>

      {todo.length === 1 || pathname === '/server' ? "" : 
      <button
        className="inline-flex items-center gap-2 rounded-md   py-2 text-sm text-gray-500 group-hover:text-red-500  focus:relative"
        onClick={deleteTodo}
      >
        <svg
          xmlns="http://www.w3.org/2000/svg"
          fill="none"
          viewBox="0 0 24 24"
          strokeWidth="1.5"
          stroke="currentColor"
          className="h-4 w-4"
        >
          <path
            strokeLinecap="round"
            strokeLinejoin="round"
            d="M14.74 9l-.346 9m-4.788 0L9.26 9m9.968-3.21c.342.052.682.107 1.022.166m-1.022-.165L18.16 19.673a2.25 2.25 0 01-2.244 2.077H8.084a2.25 2.25 0 01-2.244-2.077L4.772 5.79m14.456 0a48.108 48.108 0 00-3.478-.397m-12 .562c.34-.059.68-.114 1.022-.165m0 0a48.11 48.11 0 013.478-.397m7.5 0v-.916c0-1.18-.91-2.164-2.09-2.201a51.964 51.964 0 00-3.32 0c-1.18.037-2.09 1.022-2.09 2.201v.916m7.5 0a48.667 48.667 0 00-7.5 0"
          />
        </svg>
        Delete
      </button>
}

{pathname === '/server' &&
      <button
        className="inline-flex items-center gap-2 rounded-md   py-2 text-sm text-gray-500 group-hover:text-red-500  focus:relative"
        onClick={deleteTodo}
      >
        <svg
          xmlns="http://www.w3.org/2000/svg"
          fill="none"
          viewBox="0 0 24 24"
          strokeWidth="1.5"
          stroke="currentColor"
          className="h-4 w-4"
        >
          <path
            strokeLinecap="round"
            strokeLinejoin="round"
            d="M14.74 9l-.346 9m-4.788 0L9.26 9m9.968-3.21c.342.052.682.107 1.022.166m-1.022-.165L18.16 19.673a2.25 2.25 0 01-2.244 2.077H8.084a2.25 2.25 0 01-2.244-2.077L4.772 5.79m14.456 0a48.108 48.108 0 00-3.478-.397m-12 .562c.34-.059.68-.114 1.022-.165m0 0a48.11 48.11 0 013.478-.397m7.5 0v-.916c0-1.18-.91-2.164-2.09-2.201a51.964 51.964 0 00-3.32 0c-1.18.037-2.09 1.022-2.09 2.201v.916m7.5 0a48.667 48.667 0 00-7.5 0"
          />
        </svg>
        Delete
      </button>
}
    </div>
  );
};
