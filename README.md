<h1 align="center" id="title">Todo List - Nextjs Concept</h1>

<p align="center"><img src="https://res.cloudinary.com/djsizjaee/image/upload/v1702123548/Project/TodoList/wr3hea6up0y2oenhvik1.png" alt="project-image"></p>

<p id="description">Create a project todo list with a pure Next.js concept incorporating client components and server components. The database implementation involves using a simulated JSON Server for a fake database.</p>

<h2>Project Screenshots:</h2>

<div style='display:flex;gap:6px'>
<img src="https://res.cloudinary.com/djsizjaee/image/upload/v1702123548/Project/TodoList/wr3hea6up0y2oenhvik1.png" alt="project-screenshot" width='100%'>

<img src="https://res.cloudinary.com/djsizjaee/image/upload/v1702124081/lacp6h2nhonzwpabi0yy.png" alt="project-screenshot"  width='100%'>
</div>
<h2>🛠️ Installation Steps:</h2>

<p>1. Clone Project</p>

```
git clone https://gitlab.com/erlanggaht93/todolist
```
<p>2. setup .env & install package</p>
// for the .env structure it is in .env_example

```
npm install
```
<p>4. run the todolist project</p>
// note: the database will run itself in the new terminal 
with localhost:3001

```
npm run dev
```